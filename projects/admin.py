from django.contrib import admin
from projects.models import Project


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass


# from django.contrib import admin
# from .models import Project

# # Register your models here.
# admin.site.register(Project)


# class ProjectAdmin(admin.ModelAdmin):
#     list_display = (
#         "title",
#         "id",
#     )
